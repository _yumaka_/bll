from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('search/', views.search_result, name='search_result'),
    path('news/<slug:slug>/',views.news_detail,name='news_detail_url'),
    path('category/<slug:slug>/',views.category_detail,name='category_detail_url'),
    path('register/', views.register, name="register"),
    path('news/<slug:slug>/leave-comment', views.leave_comment, name = 'leave_comment'),
]